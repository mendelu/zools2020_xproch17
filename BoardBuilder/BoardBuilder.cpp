//
// Created by David on 23/04/2020.
//

#include "BoardBuilder.h"

void BoardBuilder::createBoard(unsigned int width, unsigned int height, TileType defaultTile){
    m_board = new Board(width, height, defaultTile);
}

Board* BoardBuilder::getBoard(){
    return m_board;
}
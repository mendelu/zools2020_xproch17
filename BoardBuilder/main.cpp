#include <iostream>

#include "BoardDirector.h"
#include "ForestBoardBuilder.h"
#include "HillBoardBuilder.h"

int main() {
    BoardDirector* director = new BoardDirector(new HillBoardBuilder());
    Board* hills = director->createBoard(15, 15);
    Board* anotherHills = director->createBoard(10, 10);

    director->setBuider(new ForestBoardBuilder());
    Board* forests = director->createBoard(15, 20);

    std::cout << "Bonus pro strelbu pole [0,0]: " << forests->getTile(0,0).shootingBonus << std::endl;
    return 0;
}

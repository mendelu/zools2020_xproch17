//
// Created by David on 23/04/2020.
//

#include "Board.h"

Board::Board(unsigned int width, unsigned int height, TileType defaultTileType){
    // vytvoreni 2D mapy

    Tile defaultTile = Tile::create(defaultTileType);
    // pomocny radek
    std::vector<Tile> tmpRow(width, defaultTile);
    // neni nejefektivnejsi
    for(int i=0; i<height; ++i) {
        m_board.push_back(tmpRow); // ukladame pomocny radek do sloupce
    }
}

void Board::setTile(Tile tile, unsigned int row, unsigned int col){
    // co kdyz nekdo zada prilis velke cislo
    m_board.at(row).at(col) = tile;
}

Tile Board::getTile(unsigned int row, unsigned int col){
    return m_board.at(row).at(col);
}
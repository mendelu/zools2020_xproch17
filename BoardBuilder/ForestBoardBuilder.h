//
// Created by David on 23/04/2020.
//

#ifndef BOARDBUILDER_FORESTBOARDBUILDER_H
#define BOARDBUILDER_FORESTBOARDBUILDER_H

#include "BoardBuilder.h"

class ForestBoardBuilder: public BoardBuilder {
public:
    void generateLandscape();
};


#endif //BOARDBUILDER_FORESTBOARDBUILDER_H

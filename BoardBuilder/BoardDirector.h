//
// Created by David on 23/04/2020.
//

#ifndef BOARDBUILDER_BOARDDIRECTOR_H
#define BOARDBUILDER_BOARDDIRECTOR_H

#include "BoardBuilder.h"

class BoardDirector {
    BoardBuilder* m_builder;
public:
    BoardDirector(BoardBuilder* builder);
    void setBuider(BoardBuilder* builder);
    Board* createBoard(unsigned int width, unsigned int height);
};


#endif //BOARDBUILDER_BOARDDIRECTOR_H

//
// Created by David on 23/04/2020.
//

#ifndef BOARDBUILDER_BOARD_H
#define BOARDBUILDER_BOARD_H

#include <vector>
#include "Tile.h"

class Board {
    std::vector<std::vector<Tile>> m_board;
public:
    Board(unsigned int width, unsigned int height, TileType defaultTileType);
    void setTile(Tile tile, unsigned int row, unsigned int col);
    Tile getTile(unsigned int row, unsigned int col);
};


#endif //BOARDBUILDER_BOARD_H

//
// Created by David on 23/04/2020.
//

#ifndef BOARDBUILDER_TILE_H
#define BOARDBUILDER_TILE_H

enum class TileType{
    Hill, Forest, Plain
};

struct Tile {
    int shootingBonus;
    int defendingBonus;

    // tovarni metoda
    static Tile create(TileType type);
};


#endif //BOARDBUILDER_TILE_H

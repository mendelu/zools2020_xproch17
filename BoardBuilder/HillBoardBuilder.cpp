//
// Created by David on 23/04/2020.
//

#include "HillBoardBuilder.h"

void HillBoardBuilder::generateLandscape() {
    // super cool random forest generator
    Tile hillTile = Tile::create(TileType::Hill);
    m_board->setTile(hillTile, 1,1);
    m_board->setTile(hillTile, 0,1);
    m_board->setTile(hillTile, 1,0);
}
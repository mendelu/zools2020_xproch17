//
// Created by David on 23/04/2020.
//

#ifndef BOARDBUILDER_BOARDBUILDER_H
#define BOARDBUILDER_BOARDBUILDER_H

#include "Board.h"

class BoardBuilder {
protected:
    Board* m_board;
public:
    void createBoard(unsigned int width, unsigned int height, TileType defaultTile);
    virtual void generateLandscape() = 0;
    Board* getBoard();
};


#endif //BOARDBUILDER_BOARDBUILDER_H

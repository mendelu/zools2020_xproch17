//
// Created by David on 23/04/2020.
//

#include "ForestBoardBuilder.h"

void ForestBoardBuilder::generateLandscape(){
    // super cool random forest generator
    Tile forestTile = Tile::create(TileType::Forest);
    m_board->setTile(forestTile, 0,0);
    m_board->setTile(forestTile, 1,1);
    m_board->setTile(forestTile, 2,2);
}

//
// Created by David on 23/04/2020.
//

#ifndef BOARDBUILDER_HILLBOARDBUILDER_H
#define BOARDBUILDER_HILLBOARDBUILDER_H

#include "BoardBuilder.h"

class HillBoardBuilder:public BoardBuilder {
public:
    void generateLandscape();
};


#endif //BOARDBUILDER_HILLBOARDBUILDER_H

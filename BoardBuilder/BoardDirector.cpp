//
// Created by David on 23/04/2020.
//

#include "BoardDirector.h"

BoardDirector::BoardDirector(BoardBuilder* builder){
    m_builder = builder;
}

void BoardDirector::setBuider(BoardBuilder* builder){
    m_builder = builder;
}

Board* BoardDirector::createBoard(unsigned int width, unsigned int height){
    // jak se vytvori board
    m_builder->createBoard(width,height, TileType::Plain);
    m_builder->generateLandscape();
    return m_builder->getBoard();
}
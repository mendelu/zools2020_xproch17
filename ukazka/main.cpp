#include <iostream>

class ManagerInterface{
public:
    virtual std::string getName() = 0;
    virtual float getPayment() = 0;
};

class TechnicianInterface{
public:
    virtual std::string getUserName() = 0;
};

class Employee: public ManagerInterface, public  TechnicianInterface{
    std::string m_name;
    std::string m_address;
    float m_payment;
    std::string m_userName;
public:
    std::string getName(){
        return m_name;
    };

    std::string getAddress(){
        return m_address;
    }

    float getPayment(){
        return m_payment;
    }

    std::string getUserName(){
        return m_userName;
    }

    Employee(std::string name, std::string address, float payment, std::string userName){
        m_name = name;
        m_address = address;
        m_payment = payment;
        m_userName = userName;
    }

    virtual ~Employee(){}

    virtual void printInfo() = 0;
};

class Manager: public Employee{
public:
    Manager(std::string name, std::string address, float payment, std::string userName):Employee(name, address, payment, userName){

    }

    void printInfoAboutEmployee(ManagerInterface* someEmployee){
        std::cout << "Name: " << someEmployee->getName() << std::endl;
        std::cout << "Payment: " << someEmployee->getPayment() << std::endl;
    }

    void printInfo(){
        std::cout << "Hello, I'm manager " << getName() << std::endl;
    }
};

class Teacher: public Employee{
    std::string m_course;
public:
    Teacher(std::string name, std::string address, float payment, std::string userName, std::string course):Employee(name, address, payment, userName){
        m_course = course;
    }

    void printInfo(){
        std::cout << "Hello, I'm teacher " << getName() << " of " << m_course << std::endl;
    }
};

class Technician: public Employee{
public:
    Technician(std::string name, std::string address, float payment, std::string userName):Employee(name, address, payment, userName){
    }

    void printInfo(){
        std::cout << "Hello, I'm technician " << getName() << std::endl;
    }
};


int main() {
    Employee* someEmployee = nullptr;
    std::cout << "Whom you want to create [M, T]: ";
    std::string input = "";
    std::cin >> input;

    //-------------------------
    if (input == "t"){
        someEmployee = new Teacher("John", "Prague", 10500.0, "xjohn", "ZOO");
    } else if (input == "m") {
        someEmployee = new Manager("James", "Prague", 10500.0, "xjames");
    } else {
        someEmployee = new Technician("Petr", "Ostrava", 30000, "xpetr");
    }
    //-------------------------

    Manager* karl = new Manager("Karl", "Brno", 100000.5, "xkarl");
    karl->printInfoAboutEmployee(someEmployee);

    someEmployee->printInfo();

    delete karl;
    delete someEmployee;
    return 0;
}

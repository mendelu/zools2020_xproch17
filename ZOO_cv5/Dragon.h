//
// Created by David on 26/03/2020.
//

#ifndef ZOO_CV5_DRAGON_H
#define ZOO_CV5_DRAGON_H

#include "Animal.h"
#include "CommunicationLogger.h"

class Dragon{
    int m_eatenCalories;
public:
    Dragon();
    void eatAnimal(int calories);
    void eatAnimal(Animal* animal);
    void printInfo();
};


#endif //ZOO_CV5_DRAGON_H

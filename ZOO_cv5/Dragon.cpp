//
// Created by David on 26/03/2020.
//

#include "Dragon.h"

Dragon::Dragon(){
    m_eatenCalories = 0;
}

void Dragon::eatAnimal(int calories){
    if(calories >= 1) {
        m_eatenCalories += calories;
        CommunicationLogger::logEvent("-", "Dragon", "calories");
        //m_eatenCalories = m_eatenCalories + calories;
    } else {
        std::cout << "Dragon::eatAnimal: Calories must be at least 1." << std::endl;
    }
}

void Dragon::eatAnimal(Animal* animal){
    if(animal != nullptr) {
        CommunicationLogger::logEvent("Animal", "Dragon", "calories");
        m_eatenCalories += animal->getCalories();
    } else {
        std::cout << "Dragon::eatAnimal: Animal must be <> from nullptr" << std::endl;
    }
}

void Dragon::printInfo(){
    std::cout << "Eaten calories: " << m_eatenCalories << std::endl;
}
//
// Created by David on 26/03/2020.
//

#include "Animal.h"

Animal::Animal(std::string name, int calories){
    setName(name);
    setCalories(calories);
}

int Animal::getCalories(){
    return m_calories;
}

std::string Animal::getName(){
    return m_name;
}

void Animal::setName(std::string name){
    if (name != ""){
        m_name = name;
    } else {
        std::cout << "Animal::setName: Empty name is not allowed. Set on default value." << std::endl;
        m_name = "Empty name";
    }
}

void Animal::setCalories(int calories){
    if (calories >= 1){
        m_calories = calories;
    } else {
        std::cout << "Animal::setCalories: Calories must be at least 1. Set on 1." << std::endl;
        m_calories = 1;
    }
}
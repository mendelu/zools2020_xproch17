#include <iostream>
#include "Animal.h"
#include "Dragon.h"

int main() {
    Animal* veverka = new Animal("Veverka", 10);
    Dragon* smak = new Dragon();
    smak->eatAnimal(veverka);

    smak->printInfo();

    delete veverka;
    delete smak;
    return 0;
}

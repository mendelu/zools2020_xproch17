//
// Created by David on 26/03/2020.
//

#ifndef ZOO_CV5_ANIMAL_H
#define ZOO_CV5_ANIMAL_H

#include <iostream>

class Animal{
    std::string m_name;
    int m_calories;
public:
    Animal(std::string name, int calories);
    int getCalories();
    std::string getName();

private:
    void setName(std::string name);
    void setCalories(int calories);
};

#endif //ZOO_CV5_ANIMAL_H

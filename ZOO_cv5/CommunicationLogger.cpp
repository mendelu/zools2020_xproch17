//
// Created by David on 26/03/2020.
//

#include "CommunicationLogger.h"

std::string CommunicationLogger::m_log = "";

void CommunicationLogger::logEvent(std::string from, std::string to, std::string what){
    m_log += from + "->" + to + ": " + what + "\n";
}

void CommunicationLogger::printEvents() {
    std::cout << "Log:" << std::endl;
    std::cout << m_log << std::endl;
}
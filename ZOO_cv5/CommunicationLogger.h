//
// Created by David on 26/03/2020.
//

#ifndef ZOO_CV5_COMMUNICATIONLOGGER_H
#define ZOO_CV5_COMMUNICATIONLOGGER_H

#include <iostream>

class CommunicationLogger{
    static std::string m_log;

    CommunicationLogger(){}
public:
    static void logEvent(std::string from, std::string to, std::string what);
    static void printEvents();
};

#endif //ZOO_CV5_COMMUNICATIONLOGGER_H

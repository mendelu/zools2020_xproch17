#include <iostream>
using namespace std;

class Parcel{
public:
    string m_sender;
    string m_receiver;
    float m_weight;
    int m_cashOnDelivery;

    Parcel(string sender, string receiver){
        m_sender = sender;
        m_receiver = receiver;
        m_weight = 0.0;
        m_cashOnDelivery = 0;
    }

    Parcel(string sender, string receiver, int cashOnDelivery){
        m_sender = sender;
        m_receiver = receiver;
        m_weight = 0.0;
        m_cashOnDelivery = cashOnDelivery;
    }

    Parcel(string sender, string receiver, float weight){
        m_sender = sender;
        m_receiver = receiver;
        setWeight(weight);
        m_cashOnDelivery = 0;
    }

    Parcel(string sender, string receiver, float weight, int cashOnDelivery){
        m_sender = sender;
        m_receiver = receiver;
        setWeight(weight);
        m_cashOnDelivery = cashOnDelivery;
    }

    void setWeight(float weight) {
        if (weight < 0) {
            cout << "Nelze mit zapornou vahu";
            m_weight = 0.0;
        } else {
            m_weight = weight;
        }
    }

    void replaceReceiverBySender(){
        m_receiver = m_sender;
    }
};

int main() {
    Parcel* toDavid = new Parcel("Petr", "David", 50.5, 150);
    cout << "Receiver: " << toDavid->m_receiver << endl;
    toDavid->replaceReceiverBySender();
    cout << "Receiver: " << toDavid->m_receiver << endl;
    
    delete toDavid;
    return 0;
}
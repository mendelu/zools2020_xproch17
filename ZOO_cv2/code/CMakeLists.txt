cmake_minimum_required(VERSION 3.14)
project(projekt)

set(CMAKE_CXX_STANDARD 14)

add_executable(projekt main.cpp)
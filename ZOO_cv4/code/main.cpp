#include <iostream>
using namespace std;

class CommunicationLogger{
    static string m_log;

    CommunicationLogger(){

    }
public:
    static void logEvent(string from, string to, string what){
        m_log += from + "->" + to + ": " + what + "\n";
    }

    static void printEvents() {
        cout << "Log:" << endl;
        cout << m_log << endl;
    }
};

// inicializace!!!
string CommunicationLogger::m_log = "";

class Animal{
    string m_name;
    int m_calories;
public:
    Animal(string name, int calories){
        setName(name);
        setCalories(calories);
    }

    int getCalories(){
        return m_calories;
    }

    string getName(){
        return m_name;
    }

private:
    void setName(string name){
        if (name != ""){
            m_name = name;
        } else {
            cout << "Animal::setName: Empty name is not allowed. Set on default value." << endl;
            m_name = "Empty name";
        }
    }

    void setCalories(int calories){
        if (calories >= 1){
            m_calories = calories;
        } else {
            cout << "Animal::setCalories: Calories must be at least 1. Set on 1." << endl;
            m_calories = 1;
        }
    }
};

class Dragon{
    int m_eatenCalories;
public:
    Dragon(){
        m_eatenCalories = 0;
    }

    void eatAnimal(int calories){
        if(calories >= 1) {
            m_eatenCalories += calories;
            CommunicationLogger::logEvent("-", "Dragon", "calories");
            //m_eatenCalories = m_eatenCalories + calories;
        } else {
            cout << "Dragon::eatAnimal: Calories must be at least 1." << endl;
        }
    }

    void eatAnimal(Animal* animal){
        if(animal != nullptr) {
            CommunicationLogger::logEvent("Animal", "Dragon", "calories");
            m_eatenCalories += animal->getCalories();
        } else {
            cout << "Dragon::eatAnimal: Animal must be <> from nullptr" << endl;
        }
    }

    void printInfo(){
        cout << "Eaten calories: " << m_eatenCalories << endl;
    }
};


int main() {
    Animal* squirrel = new Animal("Jana", 15);
    Animal* bear = new Animal("Josef", 1500);

    Dragon* karl = new Dragon();
    karl->eatAnimal( squirrel->getCalories() );
    karl->eatAnimal( bear );
    karl->printInfo();

    CommunicationLogger::printEvents();

    delete squirrel;
    delete bear;
    delete karl;
    return 0;
}
